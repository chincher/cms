<?php include "templates/include/header.php" ?>

<?php if ( $results['category'] ) { ?>
    <h1 class="categoryname"><?php echo htmlspecialchars( $results['category']->categoryname ) ?></h1>
<?php } else{
?>
    <h1>Wszystkie artykuły</h1>
<?php } ?>


<ul id="headlines" class="archive">

    <?php foreach ( $results['articles'] as $article ) { ?>

        <li>
            <h2>
                <span class="pubDate"><?php echo date('j F Y', $article->publicationDate)?></span><a href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>"><?php echo htmlspecialchars( $article->title )?></a>
            </h2>
            <?php if ( !$results['category'] && $article->category_id ) { ?>
                <span class="category">in <a href=".?action=archive&amp;category_id=<?php echo $article->category_id?>"><?php echo htmlspecialchars( $results['categories'][$article->category_id]->categoryname ) ?></a></span>
            <?php } ?>
            <p class="summary"><?php echo htmlspecialchars( $article->summary )?></p>

        </li>

    <?php } ?>

</ul>

<p><?php echo $results['totalRows']?> Artykułów<?php echo ( $results['totalRows'] != 1 ) ? '' : '' ?></p>

<p><a href="./">Powrót do strony głównej</a></p>

<?php include "templates/include/footer.php" ?>

