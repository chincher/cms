<?php include "templates/include/header.php" ?>

    <h1 style="width: 75%;"><?php echo htmlspecialchars( $results['article']->title )?></h1>
    <div style="width: 75%; font-style: italic;"><?php echo htmlspecialchars( $results['article']->summary )?></div>
    <div style="width: 75%;"><?php echo $results['article']->content?></div>
    </p>
    <p class="pubDate">Data publikacji <?php echo date('j F Y', $results['article']->publicationDate)?>
        <?php if ( $results['category'] ) { ?>
            w <a href="./?action=archive&amp;category_id=<?php echo $results['category']->id?>"><?php echo htmlspecialchars( $results['category']->categoryname ) ?></a>
        <?php } ?>
    </p>

    <p><a href="./">Powrót na stronę główną</a></p>

<?php include "templates/include/footer.php" ?>