<?php
include "templates/include/header.php";?>

<ul id="headlines" class="table table-hover">
    <?php
        foreach ($results['articles'] as $article) {
?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title">
                    <span class="pubDate"><?php echo date('j F', $article->publicationDate)?></span>  <a class="label label-info" href=".?action=viewArticle&amp;articleId=<?php echo $article->id?>" style="font-size: 20px;"><?php echo htmlspecialchars( $article->title )?></a>
            </div>
        </div>
        <div class="panel-body">
                <?php if ( $article->category_id ) { ?>
                    <span class="category">in <a class="label label-default" href=".?action=archive&amp;category_id=<?php echo $article->category_id?>"><?php echo htmlspecialchars( $results['categories'][$article->category_id]->categoryname )?></a></span>
                <?php } ?>
                <p class="summary"><?php echo htmlspecialchars( $article->summary )?></p>
            </div>
            </div>

        <?php } ?>

</ul>

    <p><a href="./?action=archive">Wszystkie artykuły</a></p>

<?php include "templates/include/footer.php" ?>