<?php include "templates/include/header.php" ?>
<?php include "templates/admin/include/header.php" ?>

    <div id="adminHeader">
        <h2>Panel zarządzania</h2>
        <p>Jesteś zalogowany jako <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Wyloguj</a></p>
    </div>

    <h1>Wszystkie artykuły</h1>

<?php if ( isset( $results['errorMessage'] ) ) { ?>
    <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>


<?php if ( isset( $results['statusMessage'] ) ) { ?>
    <div class="statusMessage  label label-success"><?php echo $results['statusMessage'] ?></div>
<?php } ?>

    <table border="1" class="table table-hover">
        <tr>
            <th>Data opublikowania</th>
            <th>Artykuł</th>
            <th>Kategoria</th>
        </tr>

        <?php foreach ( $results['articles'] as $article ) { ?>

            <tr class="art" onclick="location='admin.php?action=editArticle&amp;articleId=<?php echo $article->id?>'">
                <td><?php echo date('j M Y', $article->publicationDate)?></td>
                <td>
                    <?php echo $article->title?>
                </td>
                <td>
                    <?php echo $results['categories'][$article->category_id]->categoryname?>
                </td>
            </tr>

        <?php } ?>

    </table>

    <p><?php echo $results['totalRows']?> Liczba artykułów<?php echo ( $results['totalRows'] != 1 ) ? '' : '' ?> </p>

    <p><a href="admin.php?action=newArticle">Dodaj nowy artykuł</a></p>

<?php include "templates/include/footer.php" ?>