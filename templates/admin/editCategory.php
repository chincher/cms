<?php include "templates/include/header.php" ?>
<?php include "templates/admin/include/header.php" ?>

    <h1><?php echo $results['pageTitle']?></h1>
    <form action="admin.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="category_id" value="<?php echo $results['category']->id ?>"/>

        <?php if ( isset( $results['errorMessage'] ) ) { ?>
            <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>

        <ul>

            <li>
                <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Nazwa Kategorii</label>
                    <div class="col-sm-10">
                <input  class="form-control" type="text" name="name" id="name" placeholder="Nazwa Kategorii" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['category']->categoryname )?>" />
                    </div>
                </div>
            </li>

        </ul>

        <div class="buttons">
            <input type="submit" name="saveChanges" value="Zapisz zmiany" />
            <input type="submit" formnovalidate name="cancel" value="Anuluj" />
        </div>

    </form>

<?php if ( $results['category']->id ) { ?>
    <p><a href="admin.php?action=deleteCategory&amp;category_id=<?php echo $results['category']->id ?>" onclick="return confirm('Usunąć kategorię?')">Usuń tę kategorię</a></p>
<?php } ?>

<?php include "templates/include/footer.php" ?>