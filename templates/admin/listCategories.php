<?php include "templates/include/header.php" ?>
<?php include "templates/admin/include/header.php" ?>

    <h1>Kategorie</h1>
<?php if ( isset( $results['errorMessage'] ) ) { ?>
    <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>


<?php if ( isset( $results['statusMessage'] ) ) { ?>
    <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
<?php } ?>

    <table class="table table-hover">
        <tr>
            <th>Kategoria</th>
        </tr>

        <?php foreach ( $results['categories'] as $category ) { ?>

            <tr onclick="location='admin.php?action=editCategory&amp;category_id=<?php echo $category->id?>'">
                <td>
                    <?php echo $category->categoryname?>
                </td>
            </tr>

        <?php } ?>

    </table>

    <p><?php echo $results['totalRows']?> kategorii</p>

    <p><a href="admin.php?action=newCategory">Dodaj nową kategorię</a></p>

<?php include "templates/include/footer.php" ?>