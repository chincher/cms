<?php include "templates/include/header.php" ?>
<?php include "templates/admin/include/header.php" ?>

    <div id="adminHeader">
        <h2>Panel zarządzania</h2>
        <p>Jesteś zalogowany jako <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=logout"?>Log out</a></p>
    </div>

    <h1><?php echo $results['pageTitle']?></h1>

    <form class="form-horizontal" action="admin.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="articleId" value="<?php echo $results['article']->id ?>"/>
        <?php if ( isset( $results['errorMessage'] ) ) { ?>
            <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
        <?php } ?>

        <ul>

            <li>
                <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tytuł artykułu</label>
                    <div class="col-sm-10">
                <input type="text" class="form-control" name="title" id="title" placeholder="Nazwa artykułu" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->title )?>" />
                        </div>
                    </div>
            </li>

            <li>
                <div class="form-group">
                <label for="summary" class="col-sm-2 control-label">Opis artykułu</label>
                    <div class="col-sm-10">
                <textarea class="form-control" rows="3" name="summary" id="summary" placeholder="Zajawka do artykułu" required maxlength="1000" style="height: 5em;"><?php echo htmlspecialchars( $results['article']->summary )?></textarea>
                    </div>
                </div>

            </li>

            <li>
                <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Treść artykułu</label>
                    <div class="col-sm-10">
                <!--<textarea class="form-control" rows="4" name="content" id="content" placeholder="Treść artykułu. Może zawierać znaczniki html" required maxlength="100000" style="height: 30em;">-->
                        <textarea id="mytextarea" name="content" id="content" required maxlength="100000"><?php echo htmlspecialchars( $results['article']->content )?></textarea>
                </div>
                </div>
            </li>
            <li>
                <label for="category_id">Kategorie</label>
                <select class="form-control" name="category_id">
                    <option value="0"<?php echo !$results['article']->category_id ? " selected" : ""?>>(none)</option>
                    <?php foreach ( $results['categories'] as $category ) { ?>
                        <option value="<?php echo $category->id?>"<?php echo ( $category->id == $results['article']->category_id ) ? " selected" : ""?>><?php echo htmlspecialchars( $category->categoryname )?></option>
                    <?php } ?>
                </select>
            </li>
            <h3>SEO</h3>
            <li>
                <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tytuł artykułu</label>
                    <div class="col-sm-10">
                <input class="form-control" type="text" name="seotitle" id="seotitle" placeholder="Title" required maxlength="255" value="<?php echo htmlspecialchars( $results['article']->seotitle )?>" />
                    </div>
                </div>
            </li>
            <li>
                <div class="form-group">
                <label for="title"  class="col-sm-2 control-label">Description artykułu</label>
                    <div class="col-sm-10">
                <input class="form-control" type="text" name="seodescription" id="seodescription" placeholder="Description" required maxlength="260" value="<?php echo htmlspecialchars( $results['article']->seodescription )?>" />
                    </div>
                </div>
            </li>

            <li>
                <label for="publicationDate">Data publikacji</label>
                <input class="form-control" type="date" name="publicationDate" id="publicationDate" placeholder="YYYY-MM-DD" required maxlength="10" value="<?php echo $results['article']->publicationDate ? date( "Y-m-d", $results['article']->publicationDate ) : "" ?>" />
            </li>


        </ul>

        <div class="buttons">
            <input class="btn btn-default" type="submit" name="saveChanges" value="Zapisz zmiany" />
            <input class="btn btn-default" type="submit" formnovalidate name="cancel" value="Anuluj" />
        </div>

    </form>

<?php if ( $results['article']->id ) { ?>
    <p><a href="admin.php?action=deleteArticle&amp;articleId=<?php echo $results['article']->id ?>" onclick="return confirm('Delete This Article?')">Usuń artykuł</a></p>
<?php } ?>

<?php include "templates/include/footer.php" ?>