-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Kwi 2016, 07:13
-- Wersja serwera: 5.6.21
-- Wersja PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `cms`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
`id` tinyint(3) unsigned NOT NULL,
  `publicationDate` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `content` mediumtext NOT NULL,
  `seotitle` varchar(260) NOT NULL,
  `seodescription` varchar(260) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `articles`
--

INSERT INTO `articles` (`id`, `publicationDate`, `title`, `summary`, `content`, `seotitle`, `seodescription`, `category_id`) VALUES
(1, '2016-04-05', 'title', 'zajawka', 'content lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekstcontent lorem tekst', 'dassda', 'asdasdsda sasdasdasd', 1),
(3, '2016-04-06', 'asdasd', 'dasdd ssdaasd', 'asda assad asd asd asd jasfjasfjkasdjkasf', 'Strona artykuu', 'description artykuu', 4),
(5, '2016-04-10', 'titletitle11111', 'zajawka asdasfasf111', '<p>tresctresctresctresctresc tresctresc tresctresctresc tresc111111</p>', 'seotitletitle', 'seodesc desc desc', 1),
(6, '2016-04-21', 'asdasd', 'asdasd', 'asdasd', 'title', 'desc', 3),
(7, '2016-04-11', 'nowy title', 'nowy opis', 'nowy contentnowy contentnowy content nowy content nowy contentnowy content nowy content nowy content nowy content nowy content', 'seotitleoo!!!ooo', 'seodesc11111', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
`id` int(11) NOT NULL,
  `categoryname` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `categories`
--

INSERT INTO `categories` (`id`, `categoryname`) VALUES
(1, 'Sport'),
(3, 'Muzyka'),
(4, 'Film');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `articles`
--
ALTER TABLE `articles`
MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
