<?php

require("config.php");
$action = isset($_GET['action']) ? $_GET['action'] : "";

switch($action){
    case 'archive':
        archive();
        break;

    case 'viewArticle':
        viewArticle();
        break;

    default:
        homepage();
}

function archive(){
    $results = array();
    $category_id = ( isset( $_GET['category_id'] ) && $_GET['category_id'] ) ? (int)$_GET['category_id'] : null;
    $results['category'] = Category::getById( $category_id );
    $data = Article::getList( 100000, $results['category'] ? $results['category']->id : null );
    $results['articles'] = $data['results'];
    $results['totalRows'] = $data['totalRows'];
    $data = Category::getList();
    $results['categories'] = array();
    foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
    $results['pageHeading'] = $results['category'] ?  $results['category']->categoryname : "Article Archive";
    $results['pageTitle'] = $results['pageHeading'] ." | Brand";
    require(TEMPLATE_PATH . "/archive.php");
}

function viewArticle()
{
    if (!isset($_GET['articleId']) || !$_GET['articleId']) {
        homepage();
        return;
    }

    $results = array();
    $results['article'] = Article::getById((int)$_GET["articleId"]);
    $results['category'] = Category::getById( $results['article']->category_id );
    if(!$results['article']->seotitle == '') {
        $results['pageTitle'] = $results['article']->seotitle . " | Brand";
}else {
        $results['pageTitle'] = $results['article']->title . " | Brand";
}
    $results['pageDescription'] = $results['article']->seodescription;
    require(TEMPLATE_PATH . "/viewArticle.php");
}

function homepage(){
    $results = array();
    $data = Article::getList(HOMEPAGE_NUM_ARTICLES);
    $results['articles'] = $data['results'];
    $results['totalRows'] = $data['totalRows'];
    $data = Category::getList();
    $results['categories'] = array();
    foreach ( $data['results'] as $category ) $results['categories'][$category->id] = $category;
    $results['pageTitle'] = 'Strona główna';
    require(TEMPLATE_PATH . "/homepage.php");
}
?>

