<?php
ini_set("display_errors", false);

date_default_timezone_set("Europe/Warsaw");
define("DB_DSN", "mysql:host=localhost;dbname=cms");
define("DB_USERNAME", "root");
define( "DB_PASSWORD", "" );
define( "CLASS_PATH", "classes" );
define( "TEMPLATE_PATH", "templates" );
define( "HOMEPAGE_NUM_ARTICLES", 5 );
define( "ADMIN_USERNAME", "admin" );
define( "ADMIN_PASSWORD", "admin" );
require(CLASS_PATH . "/Article.php");
require(CLASS_PATH . "/Category.php");

function handleException($exception){
    echo "Przepraszamy, wystąpił problem. Proszę spróbować ponownie";
    error_log($exception->getMessage());
}
set_exception_handler('handleException');

?>